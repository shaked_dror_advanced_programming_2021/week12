#include "threads.h"
#include <thread>  
#include<time.h>
#include <iomanip> 
#include <chrono> 
#include <mutex>
using std::cout;
using std::endl;
using std::vector;

std::mutex mtx;

void I_Love_Threads()
{

	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{

	std::thread t(I_Love_Threads);
	t.join();
}

// O(n)
bool checkPrime(int num)
{
	if (num <= 1)
		return false;
	for (int i = 2; i <= num / 2; i++)
	{
		if (num % i == 0)
		{
			return false;
		}
	}
	return true;
}

// O(sqrt n)
bool checkPrimeEff(int num)
{
	if (num == 1)
	{
		return false;
	}
	int i = 2;
	while (i * i <= num)
	{
		if (num % i == 0)
		{
			return false;
		}
		i += 1;
	}
	return true;
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	int count = 0;
	for (int i = begin; i <= end; i++)
	{
		if (checkPrimeEff(i))
		{
			primes.push_back(i);
		}
	}
}



void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int count = 0;
	for (int i = begin; i <= end; i++)
	{
		if (checkPrime(i))
		{	
			mtx.lock();
			file << i << endl;
			mtx.unlock();
		}
	}
}

void printVector(vector<int> primes)
{
	for (vector<int>::const_iterator iter = primes.begin(); iter != primes.end(); iter++)
	{
		cout << *iter << " ";
	}
	cout << endl;
}

vector<int> callGetPrimes(int begin, int end)
{
	clock_t start, finish;
	double time_spent;
	vector<int> primes;

	start = clock();

	std::thread t(getPrimes, begin, end, ref(primes));
	t.join();


	finish = clock();
	time_spent = (double)(finish - start) / CLOCKS_PER_SEC;
	cout << begin << "-" << end << std::setprecision(5) << ": " << time_spent << " seconds" << endl;

	return primes;

}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream primes(filePath);
	clock_t start, finish;
	double time_spent;


	start = clock();

	int amount = (end - begin) / N + 1;
	std::thread* arr = new std::thread[N];
	for (int i = 0; i < N; i++)
	{
		int new_end = begin + amount * (i + 1) - 1 > end ? end : begin + amount * (i + 1) - 1;
		
		arr[i] = std::thread(writePrimesToFile, begin + amount * i, new_end, ref(primes));
		
	}
	for (int i = 0; i < N; i++)
	{
		arr[i].join();
	}

	finish = clock();
	time_spent = (double)(finish - start) / CLOCKS_PER_SEC;
	cout << begin << "-" << end << std::setprecision(5) << ": " << time_spent << " seconds" << endl;

	primes.close();
}
