#include "MessagesSender.h"
#include <iostream>
#include <thread>
#include <mutex>

int main()
{
	MessagesSender m = MessagesSender();
	std::thread to_q(&MessagesSender::from_file_by_line, std::ref(m));
	std::thread to_user(&MessagesSender::send_to_users, std::ref(m));
	m.menu();

	to_q.detach();
	to_user.detach();



	system("pause");
	return 0;
}