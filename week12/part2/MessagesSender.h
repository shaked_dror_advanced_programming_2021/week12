#pragma once
#include <iostream>
#include<set>
#include <queue>
#include <fstream>
#include <string>
#include <chrono>
#include <thread>
#include <iterator>
#include <algorithm> 
#include <thread>  
#include <chrono> 
#include <mutex>


#define FILEPATH "data.txt"
#define USERSFILE "output.txt"

class MessagesSender
{
public:
	MessagesSender();
	~MessagesSender();
	void menu();
	void signin(std::string user);
	void signout(std::string user);
	void connected_users();

	void from_file_by_line();
	void send_to_users();
	//helpers
	int get_input(int start, int end);
	std::string give_in_queue(std::queue<std::string> q,int n);
	void clear_queue(std::queue<std::string> &q);


private:
	
	std::queue<std::string> _lines;
	std::set<std::string> _users;
};