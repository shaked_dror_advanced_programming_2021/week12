#include "MessagesSender.h"
std::mutex mtx1;//to lock the use in set<std::string> _users
std::mutex mtx2;//to lock the use in queue<std::string> _lines
std::condition_variable cond;

MessagesSender::MessagesSender()
{
}
MessagesSender::~MessagesSender()
{
	this->_users.clear();
	this->clear_queue(this->_lines);
}

void MessagesSender::signin(std::string user)
{
	mtx1.lock();
	//cond.wait(locker);
	int start_len = this->_users.size();//will get the amout of users
	this->_users.insert(user);//will put the user(if he exists will not put him)
	if (start_len == this->_users.size())//if the user exists the size will be the same
	{
		std::cout << "the user " << user << " is exists" << std::endl;
	}
	mtx1.unlock();
}

void MessagesSender::signout(std::string user)
{
	mtx1.lock();
	this->_users.erase(user);
	mtx1.unlock();
}

void MessagesSender::connected_users()
{
	int i = 0;
	mtx1.lock();
	for (auto it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		if (i == 0)
		{
			std::cout << "" << *it;
			i++;
		}
		else
		{
			std::cout << ", " << *it;
		}
		
	}
	std::cout << std::endl;
	mtx1.unlock();
}

void MessagesSender::menu()
{
	int lvl = 0;
	std::string user = "";
	while (lvl != 4)
	{
		std::cout << "1-> to sign in\n2-> to sign out\n3-> to print all users\n4-> to disconnect"<< std::endl;
		lvl = get_input(1, 4);
		if (lvl == 1)
		{
			std::cout << "put a user: ";
			std::cin >> user;
			this->signin(user);
		}
		else if (lvl == 2)
		{
			std::cout << "put a user: ";
			std::cin >> user;
			this->signout(user);
		}
		else if (lvl == 3)
		{
			this->connected_users();
		}
		else
		{
			std::cout << "good by" << std::endl;
		}
	}
}

void MessagesSender::from_file_by_line()
{
	std::ofstream dfile;
	std::fstream myfile;
	std::string line;

	while (true)
	{
		myfile.open(FILEPATH);
		if (myfile.is_open())
		{
			std::unique_lock<std::mutex> locker(mtx2);
			while(getline(myfile, line))
			{		
				//std::cout << "take the line" << std::endl;//////////////////////////////////////
				this->_lines.push(line);
			}		
			locker.unlock();

			myfile.close();
			
		}
		dfile.open(FILEPATH);
		dfile.close();

		//std::cout << "w for seconds" << std::endl;//////////////////////////////////////
		cond.notify_all();

		std::this_thread::sleep_for(std::chrono::seconds(60));
	}	
}

void MessagesSender::send_to_users()
{
	std::ofstream myfile;
	//myfile.open(USERSFILE);
	std::string place;
	myfile.open(USERSFILE, std::ios_base::app); // append instead of overwrite
	while (true)
	{
		std::unique_lock<std::mutex> locker(mtx2);
		cond.wait(locker, [&]() {return !_lines.empty(); });
		for (int i = 0; i < this->_lines.size(); i++)
		{
			place = give_in_queue(this->_lines, i);
			std::unique_lock<std::mutex> locker2(mtx1);
			for (auto it = this->_users.begin(); it != this->_users.end(); ++it)
			{
				myfile << *it << ": " << place << "\n";
			}
			locker2.unlock();
		}
		this->clear_queue(this->_lines);
		locker.unlock();
	}
	myfile.close();
}


void MessagesSender::clear_queue(std::queue<std::string> &q)
{
	while (!q.empty())
	{
		q.pop();
	}
}
int MessagesSender::get_input(int start, int end)
{
	int n = 0;
	
	while (n < 1 || n>4)
	{
		std::cout << "put a choice" << std::endl;
		std::cin >> n;
	}
	return n;
}

std::string MessagesSender::give_in_queue(std::queue<std::string> q,int n)
{
	for (int i = 0; i < n; i++)
	{
		q.pop();
	}
	return q.front();
}
